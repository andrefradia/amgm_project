<?php

namespace App\Http\Controllers;

use App\Http\Requests\EducationStoreRequest;
use App\Http\Requests\EducationUpdateReques;
use App\Models\Educations;
use Illuminate\Http\Request;

class EducationsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $educations = Educations::orderBy('created_at','DESC')->get();
        return view('pendidikan.index', compact('educations'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EducationStoreRequest $request)
    {
        Educations::create($request->validated());
        return redirect('pendidikan')->with('success', 'Data Pendidikan berhasil ditambah!');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Educations $pendidikan)
    {
        return view('pendidikan.edit', compact('pendidikan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EducationUpdateReques $request, Educations $pendidikan)
    {
        $this->authorize('update', $pendidikan);
        $pendidikan->update($request->validated());
        return redirect('pendidikan')->with('success', 'Data berhasil di-edit.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Educations $educations)
    {
        $this->authorize('delete', $educations);
        $educations->delete();
        return redirect('pendidikan')->with('success', 'Data berhasil dihapus.');
    }
}
