<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EducationUpdateReques extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'min:1|max:240|alpha_num',
        ];
    }

    public function messages(): array
    {
        return [
            'name.min' => 'Minimal 1 huruf.',
            'name.max' => 'Maximal 240 huruf.',
            'name.alpha_num' => 'Hanya karakter alpha numerik.',
        ];
    }
}
