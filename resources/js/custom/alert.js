const dismissAlert = () => {
    const closealertbutton = document.querySelector('.closealertbutton');
    console.log(closealertbutton);

    closealertbutton.addEventListener('click', function(e){
        if (e.target.classList.contains('dismiss-alert')) {
            console.log(e.target.parentElement.parentElement.parentElement);
            e.target.parentElement.parentElement.parentElement.style.transition = "opacity 0.6s linear";
            e.target.parentElement.parentElement.parentElement.style.opacity = 0;
        }

    });
};

export default dismissAlert;
