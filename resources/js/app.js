import './bootstrap';

import Alpine from 'alpinejs';
import dismissAlert from './custom/alert';


window.Alpine = Alpine;

Alpine.start();
dismissAlert();
