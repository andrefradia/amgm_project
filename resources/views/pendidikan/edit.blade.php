<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Pendidikan') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('customs.flash')
            <div class="flex justify-center">
                <div class="card w-4/5 bg-base-300 shadow-xl">
                    <div class="card-body">
                        <form action="{{ route('pendidikan.update', $pendidikan->id) }}" method="post">
                        @csrf
                        @method('PUT')
                            <input name="name" type="text" placeholder="Pendidikan" class="input input-bordered w-full" value="{{ $pendidikan->name }}"/>
                            <input type="submit" value="Edit" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @vite(['resources/js/app.js'])
</x-app-layout>
