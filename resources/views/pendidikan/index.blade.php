<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pendidikan') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('customs.flash')
            <div class="flex justify-center">
                <div class="card w-4/5 bg-base-300 shadow-xl">
                    <div class="card-body">
                        <a href="{{ route('pendidikan.create') }}" class="btn btn-accent btn-sm">Tambah</a>
                    </div>
                </div>
            </div>

            @foreach ($educations as $education)
            <div class="flex justify-center my-5">
                <div class="card w-4/5 bg-base-300 shadow-xl">
                    <div class="card-body">
                        {{ $education->name }}
                        <div class="flex justify-end space-x-3">
                            @can('update', $education)
                                <a href="{{ route('pendidikan.edit', $education->id) }}" class="btn btn-info btn-sm">Edit</a>
                            @endcan
                            @can('delete', $education)
                            <form action="{{ route('pendidikan.destroy', $education->id) }}" method="post">
                            @csrf
                            @method('Delete')
                                <button type="submit" class="btn btn-error btn-sm">Delete</button>
                            </form>
                            @endcan
                            <span class="text-sm">{{ $education->created_at->diffForHumans() }}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    @vite(['resources/js/app.js'])
</x-app-layout>
